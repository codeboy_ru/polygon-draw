# -*- coding: utf-8 -*-
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

gettext = lambda s: s
DEBUG = True
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
THUMBNAIL_DEBUG = DEBUG


SECRET_KEY = 'fk4ejpmhxg#8e_r)uvj#nc4q3cd=w(*f$w@-94%0ro_^h5o9n6'

ALLOWED_HOSTS = []
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'django.contrib.gis',

    'pytils',
    # 'djangobower',
    # 'django_jenkins',
    # 'sorl.thumbnail',

    'apps_main.start_app',
    # 'apps_main.app_accounts',
    # 'apps_main.app_projects',
)

MAX_USERNAME_LENGTH = 255
MAX_EMAIL_LENGTH = 255

MIDDLEWARE_CLASSES = (
    # 'reversion.middleware.RevisionMiddleware',
    # 'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    # 'django.middleware.doc.XViewMiddleware',
    # 'django.middleware.gzip.GZipMiddleware'
)

# AUTH_USER_MODEL = 'app_accounts.User'

ROOT_URLCONF = 'project.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [PROJECT_PATH + '/templates', ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.i18n',
                'django.core.context_processors.csrf',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
            ],
        },
    },
]


SITE_ID = 1

WSGI_APPLICATION = 'wsgi.application'


LANGUAGE_CODE = 'ru'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# PROJECT_ROOT = os.path.normpath(os.path.dirname(__file__))
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',
)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_PATH, "../collected_static/")
# STATIC_ROOT = os.path.join(PROJECT_PATH, "../static/")
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, '../static'),
)

MEDIA_ROOT = os.path.join(PROJECT_PATH, "../media/")
MEDIA_URL = '/media/'

APPEND_SLASH = True


EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.mosreg.ru'
EMAIL_HOST_USER = 'othody'
EMAIL_HOST_PASSWORD = 'Gif988te'
EMAIL_PORT = 587

DEFAULT_FROM_EMAIL = 'othody@mosreg.ru'
DEFAULT_TO_EMAIL = ['iamfiery@gmail.com', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'invest_db',
        'USER': 'postgres',
        'PASSWORD': '33Cpf0kT2mEgzoE',
        'HOST': '109.95.212.114',
        'PORT': '5432',
    },
}


BOWER_COMPONENTS_ROOT = os.path.join(PROJECT_PATH, '../static/')
BOWER_INSTALLED_APPS = [
    'bootstrap#3.3.6',
    'bootstrap-datepicker',
    'bootstrap-select',
    'jquery#2.2.4',
    'ionrangeslider',
    'flot'
]


PROJECT_APPS = (
    'apps_main.start_app',
)
JENKINS_TASKS = (
    'django_jenkins.tasks.run_pep8',
    # 'django_jenkins.tasks.run_pyflakes',
    # 'django_jenkins.tasks.run_jslint',
    # 'django_jenkins.tasks.run_csslint',
    # 'django_jenkins.tasks.run_sloccount'
)

# sorl
THUMBNAIL_FORMAT = 'PNG'
THUMBNAIL_DEBUG = DEBUG
THUMBNAIL_QUALITY = 80
THUMBNAIL_PROGRESSIVE = False
THUMBNAIL_PRESERVE_FORMAT = True

try:
    from project.settings_local import *
except ImportError:
    # pass
    from project.settings_server import *
