.PHONY: requirements requirements-upgrade freeze syncdb run run-public makemessages compilemessages collectstatic cloc clean user bower dumpdata

project_name=project

requirements:
	-@echo "### Installing requirements"
	-@pip3 install -r project/requirements.txt

requirements-upgrade:
	-@echo "### Upgrading requirements"
	-@pip3 freeze | cut -d = -f 1 | xargs pip install -U

freeze:
	-@echo "### Freezing python packages to requirements.txt"
	-@pip3 freeze > project/requirements.txt

syncdb:
	-@echo "### Creating database tables and loading fixtures"
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python3 manage.py makemigrations
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python3 manage.py migrate

run:
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python3 manage.py runserver

run-public:
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python3 manage.py runserver 0.0.0.0:8000

makemessages:
	-@python3 manage.py makemessages -d djangojs --all

compilemessages:
	-@python3 manage.py compilemessages

collectstatic:
	@python3 manage.py collectstatic

user:
	-@python3 manage.py createsuperuser

bower:
	-@python3 manage.py bower_install

# If the first argument is "dumpdata"...
ifeq (dumpdata,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "dumpdata"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
dumpdata:
	@python3 manage.py dumpdata $(RUN_ARGS) --indent 4 --format json > $(RUN_ARGS).json

loaddata:
	@python3 manage.py loaddata

cloc:
	-@echo "### Counting lines of code within the project"
	-@echo "# Total:" ; find . -iregex '.*\.py\|.*\.js\|.*\.html\|.*\.css' -type f -exec cat {} + | wc -l
	-@echo "# Python:" ; find . -name '*.py' -type f -exec cat {} + | wc -l
	-@echo "# JavaScript:" ; find . -name '*.js' -type f -exec cat {} + | wc -l
	-@echo "# HTML:" ; find . -name '*.html' -type f -exec cat {} + | wc -l
	-@echo "# CSS:" ; find . -name '*.css' -type f -exec cat {} + | wc -l

clean:
	-@echo "### Cleaning *.pyc and .DS_Store files "
	-@find . -name '*.pyc' -exec rm -f {} \;
	-@find . -name '.DS_Store' -exec rm -f {} \;
