from django.conf.urls import url

from .views import Homepage, InvestorProfile


urlpatterns = [
    url(r'^$', Homepage.as_view(), name='homepage'),
    url(r'^investor-profile$', InvestorProfile.as_view(), name='investor-profile'),
]
