var ProfilePage = function () {

    var initTooltips = function () {
        $('[data-toggle="tooltip"]').tooltip()
    }

    var initBootstrapDatepicker = function () {
        $('.date-picker').datepicker({
            language: 'ru',
            orientation: "right",
            autoclose: true,
        });
    }

    var initBootstrapSelect = function () {
        $('select').selectpicker();
    }

    return {
        init: function () {
            initTooltips();
            initBootstrapDatepicker();
            initBootstrapSelect();
        }
    }
}();

$(document).ready(function() {
    ProfilePage.init();
})
