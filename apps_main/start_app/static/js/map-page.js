var MapPage = function(){

    setPageHeight = function(){
        var content_height = $(window).height() - 45 - $('header').outerHeight() - $('footer').outerHeight(),
            $portlet = $('.portlet'),
            portlet_inner_height = $portlet.find('.portlet-title').outerHeight() + $portlet.find('.portlet-body').outerHeight();


        $('.portlet').css('height', content_height + 'px');
        $('.map').css('height', content_height + 'px');

        if (portlet_inner_height > content_height) {
            $portlet.css('overflow-y', 'scroll')
        }
    }

    var initYandexMap = function(){
        var map, regions;

        var addMarker = function(position, object_id){
            // object_id -> id обьекта в бд для получения информации о нем
            // position = [55.8, 37.6]
            var placemark = new ymaps.Placemark(position);
            placemark.properties.set('object_id', object_id)
            map.geoObjects.add(placemark);

            placemark.events.add('click', function (e) {
                var object = e.get('target'),
                    object_id = object.properties.get('object_id');

                showDetail
            });
        }

        var getRegionDetail = function(region){
            var osmId = region.properties.get('osmId');

            map.setBounds(
                region.geometry.getBounds(), {
                    checkZoomRange: true,
                    strokeColor: 'rgba(54,198,211,1)'
                }
            )

            //map.geoObjects.remove(regions)

            addMarker([55.8, 37.6], 112233)

            console.log(osmId)
        }

        var getRegions = function(){

            if (regions) {
                map.geoObjects.remove(regions);
            }

            ymaps.regions.load('RU', {
                lang: 'ru',
                quality: 1
            }).then(function (result) {
                regions = result.geoObjects;

                console.log(regions)

                regions.options.set('fillColor', 'rgba(54,198,211,0)');
                regions.options.set('strokeColor', 'rgba(54,198,211,0)');

                regions.each(function (region) {

                    // Обрабока события клика на регион
                    region.events.add('click', function (e) {
                        getRegionDetail(region)
                    });

                    // Обрабока события входа курсора в регион
                    region.events.add('mouseenter', function (e) {
                        region.options.set('fillColor', 'rgba(54,198,211,0.6)');
                        region.options.set('strokeColor', 'rgba(54,198,211,1)');
                    });

                    // Обрабока события выхода курсора из региона
                    region.events.add('mouseleave', function (e) {
                        region.options.set('fillColor', 'rgba(54,198,211,0)');
                        region.options.set('strokeColor', 'rgba(54,198,211,0)');
                    });
                });

                map.geoObjects.add(regions);
            }, function (e) {
                alert('Error loading regions data.');
            });
        }

        var createYandexMap = function(){
            map = new ymaps.Map("map", {
                center: [61.881348, 63.546054],
                zoom: 2,
                behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"],
                controls: ['typeSelector', 'zoomControl']
            });

            return map;
        }

        ymaps.ready(function(){
            createYandexMap();
            getRegions();
        });
    }

    var initIonRangeSlider = function(){
        $('.range').ionRangeSlider({
            type: 'double',
            onFinish: function(result){
                $(result.input).parent().find('[data-from]').val(result.from)
                $(result.input).parent().find('[data-to]').val(result.to)
            }
        });
    }

    var initCharts = function(){
        function showChartTooltip(x, y, xValue, yValue) {
            $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 40,
                border: '0px solid #ccc',
                padding: '2px 6px',
                'background-color': '#fff'
            }).appendTo("body").fadeIn(200);
        }


        if ($('#region-statictic').size() != 0) {
            //site activities
            var previousPoint2 = null;

            var data1 = [
                ['FEB', 1100],
                ['MAR', 1200],
                ['APR', 860],
                ['MAY', 1200],
                ['JUN', 1450],
                ['JUL', 1800],
            ];


            var plot_statistics = $.plot($("#region-statictic"),

                [{
                    data: data1,
                    lines: {
                        fill: 0.2,
                        lineWidth: 0,
                    },
                    color: ['#BAD9F5']
                }, {
                    data: data1,
                    points: {
                        show: true,
                        fill: true,
                        radius: 4,
                        fillColor: "#9ACAE6",
                        lineWidth: 2
                    },
                    color: '#9ACAE6',
                    shadowSize: 1
                }, {
                    data: data1,
                    lines: {
                        show: true,
                        fill: false,
                        lineWidth: 3
                    },
                    color: '#9ACAE6',
                    shadowSize: 0
                }],

                {

                    xaxis: {
                        tickLength: 0,
                        tickDecimals: 0,
                        mode: "categories",
                        min: 0,
                        font: {
                            lineHeight: 18,
                            style: "normal",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    yaxis: {
                        ticks: 5,
                        tickDecimals: 0,
                        tickColor: "#eee",
                        font: {
                            lineHeight: 14,
                            style: "normal",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    }
                });

            $("#region-statictic").bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    console.log(123)
                    if (previousPoint2 != item.dataIndex) {
                        previousPoint2 = item.dataIndex;
                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);
                        showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                    }
                }
            });

            $('#region-statictic').bind("mouseleave", function() {
                $("#tooltip").remove();
            });
        }
    }

    var showDetailPanel = function() {
        var swithModalWindow = function(target){
            $('.modal_window').removeClass('active');
            // $(target).addClass('active');
            $(target).fadeIn(300);
        }

        var closeModalWindow = function($window){
            $window.fadeOut(300);
        }

        $('.close_modal_window').on('click', function(e){
            e.preventDefault();

            var $window = $(this).parents('.modal_window');
            closeModalWindow($window)
        })


        // $('a[href="#region"]').on('shown.bs.tab', function (e) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            if ($(e.target).attr('href') == '#region'){
                var left = $('.map').offset().left;
                $('.modal_window').css({left: left}).first().fadeIn(300);
            } else {
                $('.modal_window').fadeOut(300);
                $('.modal_details a').removeClass('active');
                $('.modal_details li:first-child a').addClass('active');
            }
        })

        $('.modal_details a').on('click', function(e){
            e.preventDefault();

            $('.modal_details a').removeClass('active');
            $(this).addClass('active');

            $('.modal_window').fadeOut(300);

            var target = $(this).attr('href');
            swithModalWindow(target)
        })
    }

    return {
        init: function(){
            initIonRangeSlider();
            initYandexMap();
            initCharts();
            setPageHeight();
            showDetailPanel();
        }
    }
}();

$(document).ready(function() {
    MapPage.init();
});
