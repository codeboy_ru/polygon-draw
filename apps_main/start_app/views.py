from django.views.generic import TemplateView


class Homepage(TemplateView):
    """
    homepage
    """
    template_name = 'test-rbounds.html'


class InvestorProfile(TemplateView):
    """
    InvestorProfile
    """
    template_name = 'investor-profile.html'
