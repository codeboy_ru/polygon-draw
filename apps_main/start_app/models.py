from django.contrib.gis.db import models

class Adm6District(models.Model):
    ogr_fid = models.IntegerField(db_column='OGR_FID', unique=True)  # Field name made lowercase.
    shape = models.MultiPolygonField(db_column='SHAPE')  # Field name made lowercase. This field type is a guess.
    id = models.FloatField(blank=True, null=True)
    osm_id = models.FloatField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    okato_code = models.CharField(max_length=11, blank=True, null=True)
    okato_name = models.CharField(max_length=255, blank=True, null=True)
    oktmo_code = models.CharField(max_length=8, blank=True, null=True)
    oktmo_name = models.CharField(max_length=255, blank=True, null=True)
    name_en = models.CharField(max_length=32, blank=True, null=True)
    name_lat = models.CharField(max_length=255, blank=True, null=True)
    area = models.FloatField(blank=True, null=True)
    adm3_id = models.FloatField(blank=True, null=True)
    adm3_name = models.CharField(max_length=255, blank=True, null=True)
    adm4_id = models.FloatField(blank=True, null=True)
    adm4_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'adm6_district'


