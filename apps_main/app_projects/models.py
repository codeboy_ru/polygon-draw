from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.contrib.gis.geos import Point
from django.contrib.gis.db import models as geo_models

# from ..app_accounts.models import User

from sorl.thumbnail import ImageField
from pygeocoder import Geocoder, GeocoderError


class FederationSubject(models.Model):
    """
    Модель хранит Субъекты РФ.
    Проверка на уникальность названия
    """
    name = models.CharField('Название', max_length=50, unique=True)
    osm_id = models.IntegerField('OSM ID', unique=True, help_text='Необходимо для Яндект карты. Для получения данных по субъектам, яндекс карта присылает нам open street map id, по которой мы осуществляем свзять')

    class Meta:
        verbose_name = 'субъект'
        verbose_name_plural = 'Субъекты РФ'

    def __str__(self):
        return self.name


class Municipality(models.Model):
    """
    Модель хранит муниципальные образования, связанные с субъектами РФ
    Проверка на уникальность названия
    """
    federation_subject = models.ForeignKey(FederationSubject, verbose_name='Субъект РФ', related_name='municipalities')
    name = models.CharField('Название', max_length=50, unique=True)

    class Meta:
        verbose_name = 'образование'
        verbose_name_plural = 'Муниципальные образования'

    def __str__(self):
        return '%s (%s)' % (self.name, self.federation_subject.__str__())


class State(models.Model):
    """
    Статус проекта (реализован, в процессе, подписано соглашение о намерении, др.)
    """
    name = models.CharField('Название', max_length=50, unique=True)

    class Meta:
        verbose_name = 'статус'
        verbose_name_plural = 'Статусы проектов'

    def __str__(self):
        return self.name


class Department(models.Model):
    """
    Отрасль проекта (металлургия, станкостроение и машиностроение, и т.д.)
    """
    name = models.CharField('Название', max_length=50, unique=True)

    class Meta:
        verbose_name = 'отрасль'
        verbose_name_plural = 'Отрасли проектов'

    def __str__(self):
        return self.name


class Project(models.Model):
    """
    Основная модель проектов
    """
    # creator = models.ForeignKey(User, verbose_name='Создатель')
    # organization = models.ForeignKey(User, verbose_name='Организация', related_name='projects')
    name = models.CharField('Название', max_length=50)
    description = models.TextField('Описание')
    municipality = models.ForeignKey(Municipality, verbose_name='Муниципальное образование')
    location = models.CharField('Местоположение', max_length=200)
    point = geo_models.PointField(blank=True, null=True, editable=False)
    start_date = models.DateField('Дата начала реализации')
    end_date = models.DateField('Дата окончания реализации')
    state = models.ForeignKey(State, verbose_name='Статус')
    department = models.ManyToManyField(Department, verbose_name='Отрасли')

    objects = geo_models.GeoManager()

    class Meta:
        verbose_name = 'проект'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return self.name


class ProjectImage(models.Model):
    """
    Изображения проектов
    """
    project = models.ForeignKey(Project, verbose_name='Проект')
    image = ImageField('Изображение')

    class Meta:
        verbose_name = 'изображение'
        verbose_name_plural = 'Изображения'

    def __str__(self):
        return str(self.pk)


@receiver(pre_save, sender=Project)
def handler_project_coordinates(sender, instance, **kwargs):
    """
    При сохранении проекта с поля местоположения получаем координаты
    и сохраняем в отдельно поле point
    """
    try:
        results = Geocoder.geocode(instance.location)
        results = results[0].coordinates
        instance.point = Point(results[0], results[1])
    except GeocoderError:
        pass
