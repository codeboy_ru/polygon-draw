from django.contrib import admin

from .models import (
    Municipality, FederationSubject, State, Department, Project, ProjectImage
)

from sorl.thumbnail.admin import AdminImageMixin


class ProjectImageAdminInline(AdminImageMixin, admin.TabularInline):
    model = ProjectImage


class ProjectAdmin(admin.ModelAdmin):
    filter_horizontal = ('department',)
    inlines = (ProjectImageAdminInline,)


admin.site.register(Municipality)
admin.site.register(FederationSubject)
admin.site.register(State)
admin.site.register(Department)
admin.site.register(Project, ProjectAdmin)
